import React, {Component} from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import imgProfile from '../../assets/icon/sell_anythign_v1.jpg';

class FlexBox extends Component {
  constructor(props) {
    super(props);
    console.log('=>> construtor');
    this.state = {
      subscriber: 200,
    };
  }

  componentDidMount() {
    console.log('==>> componentDidMount');
    setTimeout(() => {
      this.setState({
        subscriber: 400,
      });
    }, 2000);
  }

  componentDidUpdate() {
    console.log('==> componentDidUpdate');
  }

  // componentWillMount(){
  //     console.log("==>> component Will Mount")
  // }

  render() {
    console.log('render');
    return (
      <View>
        <View style={styles.wrapper}>
          <View style={styles.box} />
          <View style={styles.box} />
          <View style={styles.box} />
          <View style={styles.box} />
        </View>
        <View style={styles.menu}>
          <Text>Beranda</Text>
          <Text>Video</Text>
          <Text>PlayList</Text>
          <Text>Channel</Text>
          <Text>Tentang</Text>
        </View>
        <View style={styles.profile}>
          <Image source={imgProfile} style={styles.image} />
          <View>
            <Text style={styles.title}>Abdul Rosyid</Text>
            <Text>{this.state.subscriber} ribu Subscriber</Text>
          </View>
        </View>
      </View>
    );
  }
}
export default FlexBox;

const styles = StyleSheet.create({
  box: {
    backgroundColor: '#ee5253',
    width: 50,
    height: 50,
  },
  menu: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  wrapper: {
    flexDirection: 'row',
    backgroundColor: '#c8d6e5',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  profile: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 14,
  },
});
