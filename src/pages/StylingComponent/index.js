import React, {Component, version} from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
} from 'react-native';
import macbook from '../../assets/image/sell_anythign_v1.jpg';
const StyleingReactNativeComponent = () => {
  return (
    <View>
      <Text style={styles.text}>Styling Component</Text>
      <View
        style={{
          width: 100,
          height: 100,
          backgroundColor: '#0abde3',
          borderWidth: 2,
          marginTop: 20,
          marginLeft: 20,
        }}
      />
      <View
        style={{
          padding: 12,
          backgroundColor: '#f2f2f2',
          width: 212,
          borderRadius: 8,
        }}>
        <Image source={macbook} style={styles.imgProduct} />
        <Text style={{fontSize: 14, fontWeight: 'bold', marginTop: 12}}>
          New Baju Baru Lebaran
        </Text>
        <Text style={styles.textPrice}>Rp. 25.000.00</Text>
        <Text style={styles.textLocation}>Jakarta Barat</Text>
        <View style={styles.btnSell}>
          <Text style={styles.textBtnSell}>Beli</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#10ac84',
    marginTop: 20,
    marginLeft: 20,
  },
  textPrice: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#f2994a',
    marginTop: 12,
  },
  textLocation: {
    fontSize: 12,
    fontWeight: '300',
    marginTop: 12,
  },
  btnSell: {
    backgroundColor: '#6fcf97',
    paddingVertical: 6,
    borderRadius: 25,
    marginTop: 20,
  },
  textBtnSell: {
    fontSize: 14,
    fontWeight: '600',
    color: 'white',
    textAlign: 'center',
  },
  imgProduct: {
    width: 188,
    height: 107,
    borderRadius: 8,
  },
});

export default StyleingReactNativeComponent;
