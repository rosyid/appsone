import React, {Component} from 'react';
import {StyleSheet, View, Text, Image, TextInput} from 'react-native';
export const SampleComponent = () => {
  return (
    <View>
      <View style={{width: 80, height: 80, backgroundColor: '#0abde3'}} />
      <Text>Abdul</Text>
      <Rosyid />
      <Text>Nurhayati</Text>
      <Text>Andik</Text>
      <Photo />
      <TextInput style={{borderWidth: 1}} />
      <BoxGreen />
      <Profile />
    </View>
  );
};

const Rosyid = () => {
  return <Text>Rosyid Mangku Negoro</Text>;
};

const Photo = () => {
  return (
    <Image
      source={{uri: 'https://placeimg.com/100/100/tech'}}
      style={styles.imgProfile}
    />
  );
};

class BoxGreen extends Component {
  render() {
    return <Text>Ini Component dari class</Text>;
  }
}

class Profile extends Component {
  render() {
    return (
      <View>
        <Image
          source={{uri: 'https://placeimg.com/100/100/animals'}}
          style={styles.image}
        />
        <Text style={styles.text}>Ini Hewan</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imgProfile: {
    width: 100,
    height: 100,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  text: {
    color: 'blue',
    fontSize: 24,
  },
});
//   export default SampleComponent
